var express = require('express');
var mysql = require('mysql');

var router = express.Router();

router.post("/std-reg",function(req,res){
    const {name,uid,pwd,gender,hobbies,country,address} = req.body;
    var con = mysql.createConnection({
        host:'localhost',
        user:'root',
        password:'',
        database:'school',
        port:3306
    })
    con.connect(function(error,success){
        if(error){
            res.send('DB Connection Error')
        }
        else{
            var query = `insert into student(name,uid,pwd,gender,hobbies,country,address) values('${name}','${uid}','${pwd}','${gender}','${hobbies}','${country}','${address}')`;
            con.query(query,function(e,s){
                if(e){
                    res.send(e)
                }else{
                    res.send(s)
                }
            })
        }
    })
});
module.exports = router;

/*
url : http://localhost:2020/std/std-reg
method : post
input = {
    name:'',
    uid:'',
    pwd:'',
    gender:'',
    hobbies:'',
    country:'',
    address:''
}
*/
